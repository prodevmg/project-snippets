/**
 * This snippet represents a part of the Physical Presence tool's functionality
 * After the Lat/ Long pairs have been reverse geocoded to determine the country of each point, 
 * the resulting array is then fed into the get cities list, which asyncronously calls a geocoder object 
 * to determine the closest city. It then builds a list of the City names, grouped by unique occurrence.
 * The resulting array is then modified to include this new cities list, creating a comprehensive report of cities visited.
 * 
 */
let getCities = async (function () {

  for (let i = 0; i < results.length; i++) {

    if (results[i].cities.length > 0) {
      let points = [];
      results[i].cities.forEach( city => {
        points.push({latitude: city[0], longitude: city[1]});
      });

      geocoder.lookUp(points, (err, res) => {

          let cityNameList = [];
          res.forEach( row => {
            row.forEach( city => cityNameList.push(city.name));
          });

          let cityNamesWithoutDuplicates = cityNameList.filter(function(item, pos, arr){
            return pos === 0 || item !== arr[pos-1];
          });

          // reassign city list, and reverse so that the cities are in order from oldest to latest visits
          results[i].cities = cityNamesWithoutDuplicates.reverse();
      });
    }
  }
});