<?php

/**
 * Laravel 5 conditional validation. (inside the controller's store method)
 */

$v = Validator::make(request()->all(), [
    'location-history' => 'required|mimes:zip',
    'country' => 'required|alpha|size:2'
]);

$v->sometimes(['from-date', 'to-date'], 'required|date', function ($input) {
    return request()->get('from-date') || request()->get('to-date');
});

$v->validate();
