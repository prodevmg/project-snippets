## Query String Filtering

This folder contains a snippet from a Lesson's project, demonstrating query string filtering: Fetching database information based on request GET variables, in Laravel.

This can normally be done very simply with if-else statements right in the controller, and returning the appropriate query. But in sites where there are large amounts query string data to filter, (think Amazon/ Ebay), this method breaks down very fast.

1. Here we use a dedicated class to handle the declaration of our filter methods. 
2. We also leverage abstract classes to keep our implementation clean.
3. Use Query Scopes to allow further chaining.

Creating a flexible and readable way to implement custom Eloquent filters based on the query string.

*Inspired by Laracasts*