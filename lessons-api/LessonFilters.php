<?php

namespace App;

/**
 * Creates an easy way to add as many eloquent filter methods as needed.
 * Eloquent data by request query string:
 * http:example.com/lessons?popular&difficulty=intermediate&limit=2
 */
class LessonFilters extends QueryFilter
{
    public function popular($order = 'desc')
    {
        return $this->builder->orderBy('views', $order);
    }

    public function difficulty($level)
    {
        return $this->builder->where('difficulty', $level);
    }

    public function length($order = 'asc')
    {
        return $this->builder->orderBy('length', $order);
    }

    public function limit($count)
    {
        return $this->builder->limit($count);
    }
}
